#!/usr/bin/env python3

import requests
import sys
import json
import settings

token = settings.TOKEN_API
network = settings.NETWORK_ID
user = settings.SSH_USER

members = requests.get("https://my.zerotier.com/api/network/{0}/member".format(network), headers = {"Authorization" : "bearer {}".format(token)})
members_dict = members.json()

def config():
    for i in members_dict:
        if i['online'] == True:
            print("Host", i['name'])
            print("\t HostName ", i['config']['ipAssignments'][0])
            print("\t User {}".format(user))

def config_all():
    for i in members_dict:
        print("Host", i['name'])
        print("\t HostName ", i['config']['ipAssignments'][0])
        print("\t User {}".format(user))

def print_online():
    for i in members_dict:
        if i['online'] == True:
            print("===========================")
            print("Name:      ", i['name'])
            print("Comment:   ", i['description'])
            print("Is online: ", i['online'])
            print("intIP:     ", i['config']['ipAssignments'][0])
            print("ExtIP:     ", i['physicalAddress'])
            print("===========================")

def print_all():
    for i in members_dict:
        print("===========================")
        print("Name:      ", i['name'])
        print("Comment:   ", i['description'])
        print("Is online: ", i['online'])
        print("intIP:     ", i['config']['ipAssignments'][0])
        print("ExtIP:     ", i['physicalAddress'])
        print("===========================")

def help():
        help = """
`./curl.py all` - gives info about all members

`./curl.py config` - generates config for ssh with online members (Place in ~/.ssh/config)

`./curl.py online` - gives info about online members

`./curl.py allconfig` - generates config for ssh with all members
"""

        print(help)

def main():
    if sys.argv[1] == "all":
        print_all()
    elif sys.argv[1] == "config":
        config()
    elif sys.argv[1] == "online":
        print_online()
    elif sys.argv[1] == "allconfig":
        config_all()
    elif sys.argv[1] == "--help":
        help()


if __name__ == "__main__":
    main()
