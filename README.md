# ZeroTierMembersList

Get ZeroTier Members list via API

requiers : git , python3

# **Instalation**

`git clone https://gitlab.com/geoshapka/zerotiermemberslist.git`

`cd zerotiermemberslist`

`cp settings.py.sample settings.py`

add your settings to file "settings.py"

Get it from [my.zerotier.com](https://my.zerotier.com) (API Access Tokens) and [my.zerotier.com/network](https://my.zerotier.com/network) (Network ID)


# **Usage**

`./curl.py all` - gives info about all members

`./curl.py config` - generates config for ssh with online members (Place in ~/.ssh/config)

`./curl.py online` - gives info about online members

`./curl.py allconfig` - generates config for ssh with all members
